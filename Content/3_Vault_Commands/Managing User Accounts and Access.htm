﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="5" MadCap:lastHeight="847" MadCap:lastWidth="648">
    <head>
        <link href="../Resources/TableStyles/SimpleWithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1>Managing User Accounts and Access</h1>
        <p style="text-align: left;">Administrator responsibilities in the <MadCap:variable name="S3 Connector Variables.ComponentName" /> environment primarily involve the creation and maintenance of S3 Vault accounts for  user management and data access. </p>
        <h2>Starting the S3 Vault Module</h2>
        <p style="text-align: left;">S3 Vault modules serve as secure storage for identity- and authentication-related information with <MadCap:variable name="S3 Connector Variables.ComponentName" />s and are responsible for authentication (i.e., verifying request signatures) of requests handled by the connectors. S3 Vault supports SAML-compliant ID providers such as Microsoft ADFS to provide federated authentication/SSO services using  an external directory provider such as Active Directory.</p>
        <h2 style="text-align: left;">S3 Vault Keyfile</h2>
        <p style="text-align: left;">S3 Vault uses secure encryption to store sensitive user identity information and requires a keyfile for this purpose. As part of its key derivation process, S3 Vault uses a master key for sensible data encryption. The master key is  a string of at least 32 characters that must be placed inside the file <span class="ProprietaryFileName">roles/run-vault/files/keyfile</span> by the deploying party. If no seed exists or if the key is short, the deployment process will intentionally fail.</p>
        <p class="Note" style="text-align: left;">For an automated way to generate a master key, refer to <MadCap:xref href="../6_Operational_Tools/Playbook to Generate Master and Secret Keys.htm">"Playbook to Generate Master and Secret Keys" on page 1</MadCap:xref>.</p>
        <p style="text-align: left;">Two things must be taken into account:</p>
        <ul>
            <li>Confirm that the master key is random and long. </li>
            <p>An effective method for generating an appropriate master key:</p>
            <p class="codeparatext_lastline"> openssl rand -base64 96</p>
            <li>Redeploying with a different master key will make previously stored information unreadable. Doing this makes it impossible to generate keys previously used for encryption. So once there is a seed, store it in a safe place and  copy it to <span class="Code_Terminal">keyfile.yml</span> before deployment.</li>
        </ul>
        <h2>Starting S3 Vault with Ansible</h2>
        <p style="text-align: left;">The S3 Vault module starts automatically following installation via Ansible (refer to the <span class="PublicationName"><MadCap:variable name="S3 Connector Variables.ComponentName" /> Installation Guide</span>). If you need to restart the S3 Vault module use the <span class="Code_Terminal">ansible-playbook</span> command with the <code>&#8209;&#8209;tags</code> parameter set to <span class="ProperNameBold">vault</span>:</p>
        <p class="codeparatext_lastline">ansible-playbook -i env/{{<cite>pathToFile</cite>}}/inventory --tags vault run.yml</p>
        <p style="text-align: left;">In addition to serving as an S3 Vault module restart mechanism, this <span class="Code_Terminal">ansible-playbook</span> command places a master key file on the S3 Vault server under 400 permission and adds its path to the <code>keyFilePath</code> section of the<span class="FileName"> config.json</span> file. </p>
        <h2>Starting S3 Vault from a Command Line</h2>
        <p style="text-align: left;">When run without docker or manually within a docker container, S3 Vault can be started from a command line by running <code>npm start</code>, or alternatively <code>node vaultd.js</code>. </p>
        <p style="text-align: left;">It is necessary to generate a master key and to manually change its permissions to 400 for the user. </p>
        <p class="Note" style="text-align: left;">It is strongly recommended that S3 Vault not be started from a command line in a production system.</p>
        <h2>Setting Administration Credentials</h2>
        <p style="text-align: left;">Only requests using pre-deployed administration credentials are granted access to administration routes  (e.g., create, delete, and list for accounts). </p>
        <p class="Note" style="text-align: left;">For an automated way to generate administration credentials, refer to <MadCap:xref href="../6_Operational_Tools/Playbook to Generate Admin Credentials.htm">"Playbook to Generate Administration Credentials" on page 1</MadCap:xref>.</p>
        <p style="text-align: left;">To set administration credentials manually, create a file on the filesystem and indicate its path in the entry adminCredentialsFilePath of the Vault <span class="FileName">config.json</span> file. The file must contain a json structure with the desired credentials. For example:</p>
        <p class="codeparatext">{</p>
        <p class="codeparatext_indent">"{{accessKey1}}": "{{secretKey1}}",</p>
        <p class="codeparatext_indent">"{{accessKey2}}": "{{secretKey2}}"</p>
        <p class="codeparatext_lastline">}</p>
        <p style="text-align: left;">AccessKeys must be between 16 and 32 characters (A-Z0-9), while secretKeys must be 40 characters (a-zA-Z0-9/+=). The credentials can be randomly generated using the following commands:</p>
        <p class="codeparatext"># accessKey</p>
        <p class="codeparatext">$ cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 20| head -1</p>
        <p class="codeparatext"># secretKey</p>
        <p class="codeparatext">$ cat /dev/urandom | tr -dc 'a-zA-Z0-9/=+' | fold -w 40| head -1</p>
        <h2 MadCap:conditions="">Configuring HTTPS for Inter-Component Communication</h2>
        <p MadCap:conditions="" style="text-align: left;">The HTTPS protocol is optional. If using HTTPS for inter-component communication, it is necessary to provide at least a certificate and a private key file to configure HTTPS. It is highly recommended that the certificate be signed by a well-known Certification Authority (CA), in which case the path to the CA's certificate file can also be configured.</p>
        <p MadCap:conditions="" style="text-align: left;">Configure HTTPS&#160;for inter-component communication by uncommenting and setting the following parameters in the <cite>env/{{my_setup_env}}/group_vars/all</cite> template (Refer to the S3 Installation Guide for additional information about the group variables template):</p>
        <p class="codeparatext"># env_tls_cert: /tmp/testcert/cert.pem</p>
        <p class="codeparatext"># env_tls_key: /tmp/tescert/key.pem</p>
        <p class="codeparatext_lastline"># env_tls_ca: /tmp/testcert/ca.pem</p>
        <p class="Note" style="text-align: left;">The certificate files must be deployed to the same path on all target machines and have the same names.</p>
        <p MadCap:conditions="" style="text-align: left;">If the certificate file paths in the group variables template are set before installation, the <cite>config.json</cite> file is automatically updated with the entered information.</p>
        <h2>Enabling SAML Support in Vault</h2>
        <p style="text-align: left;">Enable SAML support in Vault by uncommenting and setting the <code>env_vault_saml</code> section variables in the <cite>env/{{my_setup_env}}/group_vars/all</cite> template file before installing or updating the <MadCap:variable name="S3 Connector Variables.ComponentName" />.</p>
        <p style="orphans: 2;text-align: left;">The following variables can be set for SAML support in the template file:</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/SimpleWithPadding.css');" class="TableStyle-SimpleWithPadding" cellspacing="0">
            <col class="TableStyle-SimpleWithPadding-Column-Column1" style="width: 130pt;" />
            <col class="TableStyle-SimpleWithPadding-Column-Column2" style="width: 280px;" />
            <col class="TableStyle-SimpleWithPadding-Column-Column3" style="width: 140pt;" />
            <thead>
                <tr class="TableStyle-SimpleWithPadding-Head-Header1">
                    <th class="TableStyle-SimpleWithPadding-HeadE-Column1-Header1">Variable</th>
                    <th class="TableStyle-SimpleWithPadding-HeadE-Column2-Header1">Description</th>
                    <th class="TableStyle-SimpleWithPadding-HeadD-Column3-Header1">Default (if uncommented)</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">entry_point</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">ID&#160;provider's URL</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">—</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">entry_point_logout</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">(Optional) ID&#160;provider's logout URL</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">ADFS</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">issuer:<br />&#160;host<br /></td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">IP address of Vault interface to single sign-on users</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">https://127.0.0.1</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">issuer:<br />&#160;port</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">Port for the Vault interface to single sign-on users</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">3030</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">secretkey_expiration_hours</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">Number of hours during which the retrieved accessKey and secretKey are valid</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">1</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">session_expiration_hours</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">Number of hours during which a session is valid</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">6</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">tls_cert</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">(Required if using HTTPS for SAML connections) Route to the HTTPS&#160;certificate file on the target machines</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">/tmp/testcert/cert.pem</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">tls_key</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">(Required if using HTTPS for SAML connections) Route to the HTTPS cerificate private key on the target machines</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">/tmp/testcert/key.pem</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">account_name_claim</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">Name of the field containing the account name in ADFS&#160;or in SAML 2.0 implementations</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">accountname</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">account_address_claim</td>
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column2-Body1">Name of the field containing the account email address in ADFS or in SAML 2.0 implementations</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column3-Body1">emailaddress</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyB-Column1-Body1">primary_group_sid_claim</td>
                    <td class="TableStyle-SimpleWithPadding-BodyB-Column2-Body1">Name of the field containing the ID for the user an ADFS&#160;or in SAML 2.0 implementations</td>
                    <td class="TableStyle-SimpleWithPadding-BodyA-Column3-Body1">primarygroupsid</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>